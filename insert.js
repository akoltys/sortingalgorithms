function drawInsert() {
    var parentName = 'insertValues';
    var nodeClass = 'value';
    var valuesarea = document.getElementById(parentName);
    console.debug(gDataToSortInsert);
    var nodeSorted = document.createElement("div");
    nodeSorted.className = 'insertPart';
    gInsertSortedValues.forEach(value => {
        var node = document.createElement("div");
        node.className = nodeClass;
        node.innerText = value;
        nodeSorted.appendChild(node);
    });
    valuesarea.appendChild(nodeSorted);

    console.debug(gInsertSortedValues);
    var nodeUnSorted = document.createElement("div");
    nodeUnSorted.className = 'insertPart';
    gDataToSortInsert.forEach(value => {
        var node = document.createElement("div");
        node.className = nodeClass;
        node.innerText = value;
        nodeUnSorted.appendChild(node);
    });
    valuesarea.appendChild(nodeUnSorted);

    console.debug('*******');
}

var gInsertSortedValues = []

function redrawInsert() {
    cleanValues('insertValues');
    drawInsert();
}

async function doInsertSort(steps) {
    if (gDataToSortInsert.length == 0) {
        if (gDataToSort.length == 0) {
            return;
        }
        gDataToSortInsert = [];
        gDataToSort.forEach(element => {
            gDataToSortInsert.push(element);
        });
        gInsertSortedValues = [];
        redrawSelect();
    }
    console.debug(gDataToSortInsert);
    while (gDataToSortInsert.length > 0) {
        var tmpVal = gDataToSortInsert.shift();
        var inserted = false;
        for (var idx = 0; idx < gInsertSortedValues.length; idx++) {
            if (tmpVal < gInsertSortedValues[idx]) {
                gInsertSortedValues.splice(idx, 0, tmpVal);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            gInsertSortedValues.push(tmpVal);
        }

        redrawInsert();
        await sleep(500);
        if (steps != 0) {
            return;
        }
    }
    console.debug("Finished insert sorting");
}
