function redrawTree(currentNode, currentElement) {
    if (currentNode >= gHeapSize) {
        return;
    }
    var node = document.createElement("div");
    node.className = "node";
    node.innerText = gDataToSortHeap[currentNode];
    var leftNode = document.createElement("div");
    var rightNode = document.createElement("div");
    leftNode.className = "left";
    rightNode.className = "right";
    currentElement.appendChild(node);
    var left = (currentNode * 2) + 1;
    var right = (currentNode * 2) + 2;
    
    if (left < gHeapSize) {
        var branch = document.createElement("div");
        branch.className ="treeBranchLeft";
        currentElement.appendChild(leftNode);
        leftNode.appendChild(branch);
        redrawTree(left, leftNode);
    } else if (right < gHeapSize) {
        var branch = document.createElement("div");
        branch.className ="treeBranchLeft";
        currentElement.appendChild(leftNode);
        leftNode.appendChild(branch);
    }
    if (right < gHeapSize) {
        var branch = document.createElement("div");
        branch.className ="treeBranchRight";
        currentElement.appendChild(rightNode);
        rightNode.appendChild(branch);
        redrawTree(right, rightNode);
    }
}

function drawHeap() {
    console.debug('Heap draw');
    var parentName = 'heapValues';
    var valuesarea = document.getElementById(parentName);
    var tree = document.createElement("div");
    tree.id = "heapTree";
    var arr = document.createElement("div");
    tree.id = "heapArray";
    valuesarea.appendChild(arr);
    valuesarea.appendChild(tree);
    drawValues('heapArray', 'value', gDataToSortHeap);
    redrawTree(0, tree);
}

function redrawHeap() {
    cleanValues('heapValues');
    drawHeap();
}

const HeapStage = {
    IDLE : 0,
    HEAP : 1,
    SORT : 2,
}

var gHeapSortState = HeapStage.IDLE;
var gHeapSize = 0;

function swap(arr, idxa, idxb) {
    var tmp = arr[idxa];
    arr[idxa] = arr[idxb];
    arr[idxb] = tmp;
}

function heapify(arr, idx, size) {
    var newMax = idx;
    var childL = 2*idx+1;
    var childR = 2*idx+2;
    if (childL < size && arr[childL] > arr[newMax]) {
        newMax = childL;
    }
    if (childR < size && arr[childR] > arr[newMax]) {
        newMax = childR;
    }
    if (newMax != idx) {
        swap(arr, idx, newMax);
        heapify(arr, newMax, size);
    }
}

async function doHeapSort(steps) {
    console.debug("Start Insert sorting")
    if (gDataToSortHeap.length == 0 || gHeapSortState == HeapStage.IDLE) {
        if (gDataToSort.length == 0) {
            return;
        }
        gDataToSortHeap = [];
        gDataToSort.forEach(element => {
            gDataToSortHeap.push(element);
        });
        gHeapSortState = HeapStage.HEAP;
    }

    console.debug("Star making heap");
    for(var idx = Math.floor(gDataToSortHeap.length/2); idx >= 0; idx--) {
        console.debug("Idx: ", idx, " Checking: ", gDataToSortHeap[idx]);
        heapify(gDataToSortHeap, idx, gDataToSortHeap.length);
        console.debug(gDataToSortHeap);
        gHeapSize = gDataToSortHeap.length;
        redrawHeap();
        await sleep(500);
        if (idx == 0) {
            gHeapSortState = HeapStage.HEAP;
        }
    }
    console.debug("Done making heap");
    console.debug(gDataToSortHeap);
    
    console.debug("Star sorting");
    for(var idx = gDataToSortHeap.length-1; idx > 0; idx--) {
        swap(gDataToSortHeap, 0, idx);
        heapify(gDataToSortHeap, 0, idx);
        console.debug(gDataToSortHeap);
        gHeapSize = idx;
        redrawHeap();
        await sleep(500);
    }
    console.debug("Done sorting");
    console.debug(gDataToSortHeap);
    gHeapSize = 0;
    redrawHeap();
    gHeapSortState = HeapStage.IDLE;
    console.debug("Finished Insert sorting");
}
