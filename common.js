var gDataToSort = [];

// Bubble sort global variables
var gDataToSortBubble = [];

// Select sort global variables
var gDataToSortSelect = [];

// Insert sort global variables
var gDataToSortInsert = [];

// Merge sort global variables
var gDataToSortMerge = [];

// Heap sort global variables
var gDataToSortHeap = [];

// Bucket sort global variables
var gDataToSortBucket = [];

// Bucket sort global variables
var gDataToSortRadix = [];

// Bucket sort global variables
var gDataToSortQuick = [];

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

function drawValues(parentName, nodeClass, nodesData) {
    var valuesarea = document.getElementById(parentName);
    for (value in nodesData) {
        var node = document.createElement("div");
        node.className = nodeClass;
        node.innerText = nodesData[value];
        valuesarea.appendChild(node);
    }
}

function cleanValues(parentName) {
    var valuesarea = document.getElementById(parentName);
    while (valuesarea.firstChild) {
        valuesarea.removeChild(valuesarea.lastChild);
    }
}

function refreshValues() {
    cleanValues('values');
    drawValues('values', 'value' , gDataToSort);
}

function addValue() {
    var newValue = Number(document.getElementById('newValue').value);
    if (newValue == NaN || newValue > 100 || newValue < 0) {
        console.debug("Invlaid value.");
        return;
    }
    console.debug("New value to add: ", newValue)
    gDataToSort.push(newValue);
    refreshValues();
}

function generateValues() {
    gDataToSort = [];
    for(var idx = 0; idx < 15; idx++) {
        gDataToSort.push(Math.floor(Math.random() * 10001));
    }
    refreshValues();
}

function clearValues() {
    gDataToSort = [];
    gDataToSortBubble = [];
    gDataToSortMerge = [];
    gDataToSortSelect = [];
    gDataToSortInsert = [];
    gDataToSortHeap = [];
    gDataToSortBucket = [];
    gDataToSortRadix = [];
    gDataToSortQuick = [];
    cleanValues('values');
    cleanValues('bubbleValues');
    cleanValues('mergeValues');
    cleanValues('selectValues');
    cleanValues('insertValues');
    cleanValues('heapValues');
    cleanValues('bucketValues');
    cleanValues('radixValues');
    cleanValues('quickValues');
}

function restOfArrayIsSorted(startIdx, valuesArray) {
    if ((idx + 1) >= valuesArray.length) {
        return true;
    }
    for (var idx = startIdx+1; idx < valuesArray.length; idx++) {
        if (valuesArray[idx-1] > valuesArray[idx]) {
            return false;
        }
    }
    return true;
}
