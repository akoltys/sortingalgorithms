function drawRadix() {
    drawValues('radixValues', 'value', gRadixSortArr[0]);
    drawValues('radixValues', 'value', gRadixSortArr[1]);
}

function redrawRadix() {
    cleanValues('radixValues');
    drawRadix();
}

const RadixStage = {
    IDLE : 0,
    SORT : 1,
}

gRadixSortState = RadixStage.IDLE;
gRadixSortArr = undefined;
gRadixBitNo = 0;

async function doRadixSort(steps) {
    console.debug("Radix sort START");
    if (gDataToSortRadix.length == 0 || gRadixSortState == RadixStage.IDLE) {
        if (gDataToSort.length == 0) {
            return;
        }
        gDataToSortRadix = [];
        gDataToSort.forEach(element => {
            gDataToSortRadix.push(element);
        });
        gRadixSortState = RadixStage.SORT;
        gRadixBitNo = 0;
    }

    for (var bitNo = gRadixBitNo; bitNo < 32; bitNo++) {
        var tmpArr = [[], []];
        var mask = 0x1<<bitNo;
        var found = false;
        gRadixBitNo++;
        if (bitNo == 0) {
            gDataToSortRadix.forEach(element => {
                var val = element&mask;
                if (val == 0) {
                    found = true;
                    tmpArr[0].push(element);
                } else {
                    tmpArr[1].push(element);
                }
            });
        } else {
            for (var idx = 0; idx < 2; idx++) {
                gRadixSortArr[idx].forEach(element => {
                    var val = element&mask;
                    if (val == 0) {
                        found = true;
                        tmpArr[0].push(element);
                    } else {
                        tmpArr[1].push(element);
                    }
                });
            }
        }
        gRadixSortArr = tmpArr;
        console.debug(gRadixSortArr[0]);
        console.debug(gRadixSortArr[1]);
        redrawRadix();
        await sleep(500);
        if (found == false) {
            break;
        }
        if (steps != 0) {
            return;
        }
    }
    gDataToSortRadix = gRadixSortArr[0];
    gRadixSortState = RadixStage.IDLE;
    redrawRadix();
    console.debug(gDataToSortRadix);
    console.debug("Radix sort STOP");
}
