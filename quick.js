function drawQuick() {
    //drawValues('quickValues', 'value', gDataToSortQuick);
    var parentName = 'quickValues';
    var nodeClass = 'value';
    var valuesarea = document.getElementById(parentName);
    console.debug(gStopIdx, gStopIdx, gMiddleIdx);
    for (var value = 0; value < gDataToSortQuick.length; value++) {
        var node = document.createElement("div");
        node.className = nodeClass;
        if (gQuickSortState != QuickStage.IDLE) {
            if (value == gMiddleIdx) {
                node.className = 'quickMiddleVal';
            } else if (value == gStartIdx || value == (gStopIdx-1)) {
                node.className = 'quickBorderVal';
            }
        }
        node.innerText = gDataToSortQuick[value];
        valuesarea.appendChild(node);
    }
}

function redrawQuick() {
    cleanValues('quickValues');
    drawQuick();
}

const QuickStage = {
    IDLE : 0,
    SORT : 1,
}

var gToCheck = [];
var gStartIdx = 0;
var gStopIdx = 0;
var gMiddleIdx = 0;

function swapQuick(arr, start, stop) {
    var result = [];
    var middlepos = start;
    var middle = Math.floor((start+stop)/2);
    result.push(arr[middle]);
    for (var idx = start; idx < stop; idx++) {
        if (idx != middle) {
            if (arr[idx] < arr[middle]) {
                result.unshift(arr[idx]);
                middlepos++;
            } else {
                result.push(arr[idx]);
            }
        }
    }
    //arr.splice(start, result.length, result);
    Array.prototype.splice.apply(arr, [start, result.length].concat(result));
    return middlepos;
}

async function doQuickSort(steps) {
    console.debug("Quick sort START");
    if (gDataToSortQuick.length == 0 || gQuickSortState == QuickStage.IDLE) {
        if (gDataToSort.length == 0) {
            return;
        }
        gDataToSortQuick = [];
        gDataToSort.forEach(element => {
            gDataToSortQuick.push(element);
        });
        gToCheck = [[0, gDataToSortQuick.length]];
        gQuickSortState = QuickStage.SORT;
    }

    while (gToCheck.length > 0) {
        var tmp = gToCheck.pop();
        gStartIdx = tmp[0];
        gStopIdx = tmp[1];
        if (gStopIdx > gStartIdx) {
            gMiddleIdx = swapQuick(gDataToSortQuick, gStartIdx, gStopIdx);
            if(gMiddleIdx > gStartIdx) {
                gToCheck.push([gStartIdx, gMiddleIdx]);
            }
            if (gMiddleIdx < gStopIdx) {
                gToCheck.push([gMiddleIdx+1, gStopIdx]);
            }
        }
        if (gMiddleIdx != 0) {
            redrawQuick();
            await sleep(500);
        }
        if (steps != 0) {
            return;
        }
    }
    gQuickSortState = QuickStage.IDLE;
    redrawQuick();
    console.debug("Quick sort STOP");
}
