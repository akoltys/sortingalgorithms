function drawBucket() {
    var parentName = 'bucketValues';
    var nodeClass = 'value';
    var valuesarea = document.getElementById(parentName);
    var bucketArr = document.createElement("div");
    console.debug(gBucketArray);
    gBucketArray.forEach(element => {
        console.debug("Bucket Array: ", element);
        if (Array.isArray(element)) {
            var nodeArr = document.createElement("div");
            nodeArr.className = 'bucketPart';
            element.forEach(value => {
                var node = document.createElement("div");
                node.className = nodeClass;
                node.innerText = value;
                nodeArr.appendChild(node);
            });
            bucketArr.appendChild(nodeArr);
        } else {
            var node = document.createElement("div");
            node.className = nodeClass;
            node.innerText = element;
            bucketArr.appendChild(node);
        }
    });
    valuesarea.appendChild(bucketArr);
    drawValues('bucketValues', 'value', gDataToSortBucket);
}

function redrawBucket() {
    cleanValues('bucketValues');
    drawBucket();
}

const BucketStage = {
    IDLE: 0,
    BUCKET: 1,
    SORT: 2,
    FINISH: 3,
}

gBucketArray = [];
gBucketSize = 0;
gBucketIdx = 0;
gBucketMax = undefined;
gBucketSortState = BucketStage.IDLE;

function getMin(data) {
    if (data.length > 0) {
        var val = data[0];
        var pos = 0;
        for (var idx = 1; idx < data.length; idx++) {
            if (data[idx] < val) {
                val = data[idx];
                pos = idx;
            }
        }
        return data.splice(pos, 1)[0];
    } else {
        return undefined;
    }
}

async function doBucketSort(steps) {
    console.debug("Bucket sort START");
    if ((gDataToSortBucket.length == 0 && gBucketSortState != BucketStage.SORT) 
            || gBucketSortState == BucketStage.IDLE) {
        if (gDataToSort.length == 0) {
            return;
        }
        gBucketMax = undefined;
        gDataToSortBucket = [];
        gBucketArray = [];
        gDataToSort.forEach(element => {
            if (gBucketMax == undefined || element > gBucketMax) {
                gBucketMax = element;
            }
            gDataToSortBucket.push(element);
            gBucketArray.push([]);
        });
        gBucketIdx = 0;
        gBucketMax += 1;
        gBucketSize = gDataToSort.length;
        gBucketSortState = BucketStage.BUCKET;
    }
    if (gBucketSortState == BucketStage.BUCKET) {
        console.debug("Data len: ", gBucketSize, " Max val: ", gBucketMax);
        for (var idx = gBucketIdx; idx < gDataToSortBucket.length; idx++) {
            gBucketIdx++;
            var bucket = Math.floor(gDataToSortBucket[idx]*gBucketSize/gBucketMax);
            gBucketArray[bucket].push(gDataToSortBucket[idx]);
            redrawBucket();
            await sleep(500);
            if(steps != 0) {
                return;
            }
        }
        gBucketIdx = 0;
        gBucketSortState = BucketStage.SORT;
        console.debug(gBucketArray);
    }
    
    if (gBucketSortState == BucketStage.SORT) {
        if (gBucketIdx == 0) {
            gDataToSortBucket = [];
        }
        for (var idx = gBucketIdx; idx < gBucketArray.length; idx++) {
            if (gBucketArray[idx].length == 0) {
                continue;
            }
            gBucketIdx++;
            var tmp = getMin(gBucketArray[idx]);
            while (tmp != undefined) {
                gDataToSortBucket.push(tmp);
                var tmp = getMin(gBucketArray[idx]);
                redrawBucket();
                await sleep(500);
            }
            redrawBucket();
            await sleep(500);
            if (steps != 0) {
                return;
            }
        }
    }
    gBucketSortState = BucketStage.IDLE;
    console.debug(gDataToSortBucket);
    console.debug("Bucket sort STOP");
}
