const MergeStage = {
    DIVIDING : 0,
    MERGING : 1,
    IDLE : 2,
}

var gMergeState = MergeStage.IDLE;

function drawMerge() {
    var parentName = 'mergeValues';
    var nodeClass = 'value';
    var valuesarea = document.getElementById(parentName);
    console.debug(gDataToSortMerge);
    gDataToSortMerge.forEach(element => {
        console.debug("Merge Array: ", element);
        if (Array.isArray(element)) {
            var nodeArr = document.createElement("div");
            nodeArr.className = 'mergePart';
            element.forEach(value => {
                var node = document.createElement("div");
                node.className = nodeClass;
                node.innerText = value;
                nodeArr.appendChild(node);
            });
            valuesarea.appendChild(nodeArr);
        } else {
            var node = document.createElement("div");
            node.className = nodeClass;
            node.innerText = element;
            valuesarea.appendChild(node);
        }
    });
    console.debug('***')
}

function redrawMerge() {
    cleanValues('mergeValues');
    drawMerge();
}

async function doMergeSort(steps) {
    if (gDataToSortMerge.length == 0 || gMergeState == MergeStage.IDLE) {
        if (gDataToSort.length == 0) {
            return;
        }
        gDataToSortMerge = [];
        gDataToSort.forEach(element => {
            gDataToSortMerge.push(element);
        });
        gMergeState = MergeStage.DIVIDING;
    }

    while(gMergeState == MergeStage.DIVIDING) {
        var divideFinished = false;
        console.debug("First el: ", Array.isArray(gDataToSortMerge[0]));
        if (Array.isArray(gDataToSortMerge[0]) && gDataToSortMerge[0].length == 1) {
            divideFinished = true;
        } else {
            var tmp = [];
            if (!Array.isArray(gDataToSortMerge[0])) {
                tmp.push(gDataToSortMerge.slice(0, (gDataToSortMerge.length+1)/2));
                tmp.push(gDataToSortMerge.slice((gDataToSortMerge.length+1)/2, gDataToSortMerge.length));
            } else {
                gDataToSortMerge.forEach((element) => {
                    if (element.length > 1) {
                        console.debug("Element: ", element);
                        tmp.push(element.slice(0, (element.length+1)/2));
                        tmp.push(element.slice((element.length+1)/2, element.length));
                    } else {
                        tmp.push(element);
                    }
                });
            }
            gDataToSortMerge = tmp;
        }
        redrawMerge();
        await sleep(500);
        if (divideFinished) {
            console.debug("Finished divide phase");
            gMergeState = MergeStage.MERGING;
        }
        if (steps != 0) {
            return;
        }
    }
    while(gMergeState == MergeStage.MERGING) {
        var tmpMerge = [];
        for (var idx = 0; idx < gDataToSortMerge.length; idx += 2) {
            if (idx+1 < gDataToSortMerge.length) {
                var tmp = [];
                var parta = gDataToSortMerge[idx];
                var partb = gDataToSortMerge[idx+1];
                while (parta.length > 0 || partb.length > 0) {
                    if (partb.length == 0) {
                        tmp.push(parta.shift());
                    }
                    else if (parta.length == 0) {
                        tmp.push(partb.shift());
                    }
                    else if (parta[0] <= partb[0]) {
                        tmp.push(parta.shift());

                    } else {
                        tmp.push(partb.shift());
                    }
                }
                tmpMerge.push(tmp);
            } else {
                tmpMerge.push(gDataToSortMerge[idx]);
            }
        }
        if (tmpMerge.length > 1) {
            gDataToSortMerge = tmpMerge;
        } else {
            gDataToSortMerge = tmpMerge[0];
        }
        redrawMerge();
        await sleep(1000);
        if (!Array.isArray(gDataToSortMerge[0])) {
            console.debug("Finished merge phase");
            gMergeState = MergeStage.IDLE;
            return;
        }
        if (steps != 0) {
            return;
        }
    }
}
