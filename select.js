function drawSelect() {
    var parentName = 'selectValues';
    var nodeClass = 'value';
    var valuesarea = document.getElementById(parentName);
    console.debug(gDataToSortSelect);
    var nodeSorted = document.createElement("div");
    nodeSorted.className = 'selectPart';
    gSelectSortedValues.forEach(value => {
        var node = document.createElement("div");
        node.className = nodeClass;
        node.innerText = value;
        nodeSorted.appendChild(node);
    });
    valuesarea.appendChild(nodeSorted);

    console.debug(gSelectSortedValues);
    var nodeUnSorted = document.createElement("div");
    nodeUnSorted.className = 'selectPart';
    gDataToSortSelect.forEach(value => {
        var node = document.createElement("div");
        node.className = nodeClass;
        node.innerText = value;
        nodeUnSorted.appendChild(node);
    });
    valuesarea.appendChild(nodeUnSorted);

    console.debug('*******');
}

var gSelectSortedValues = []

function redrawSelect() {
    cleanValues('selectValues');
    drawSelect();
}

function getMinValueSelect() {
    var result = gDataToSortSelect[0];
    var ridx = 0;
    for(var idx = 1; idx < gDataToSortSelect.length; idx++) {
        if (result > gDataToSortSelect[idx]) {
            ridx = idx;
            result = gDataToSortSelect[idx];
        }
    }
    return gDataToSortSelect.splice(ridx, 1)[0];
}

async function doSelectSort(steps) {
    if (gDataToSortSelect.length == 0) {
        if (gDataToSort.length == 0) {
            return;
        }
        gDataToSortSelect = [];
        gDataToSort.forEach(element => {
            gDataToSortSelect.push(element);
        });
        gSelectSortedValues = [];
        redrawSelect();
    }
    console.debug(gDataToSortSelect);
    while (gDataToSortSelect.length > 0) {
        var tmpMin = getMinValueSelect();
        gSelectSortedValues.push(tmpMin);
        redrawSelect();
        await sleep(500);
        if (steps != 0) {
            return;
        }
    }
    console.debug("Finished select sorting");
}
