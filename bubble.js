var gBubbleSortLastIdx = 0;

function drawBubbles() {
    drawValues('bubbleValues', 'value', gDataToSortBubble);
}

function redrawBubbles() {
    cleanValues('bubbleValues');
    drawBubbles();
}

async function doBubbleSort(steps) {
    if (gDataToSortBubble.length == 0) {
        if (gDataToSort.length == 0) {
            return;
        }
        gDataToSortBubble = [];
        gDataToSort.forEach(element => {
            gDataToSortBubble.push(element);
        });
        gBubbleSortLastIdx = 0;
    }

    var idx = 0;
    if (steps != 0) {
        idx = gBubbleSortLastIdx;
    }
    var vals = gDataToSortBubble;
    while(!restOfArrayIsSorted(idx, vals)) {
        for (val in vals) {
            val = Number(val);
            if (val > idx) {
                if (vals[idx] > vals[val]) {
                    var tmp = vals[val];
                    vals[val] = vals[idx];
                    vals[idx] = tmp;
                    idx = Number(val);
                    if (steps != 0) {
                        if (!restOfArrayIsSorted(idx, gDataToSortBubble)) {
                            gBubbleSortLastIdx = idx;
                        } else {
                            gBubbleSortLastIdx = 0;
                        }
                        redrawBubbles();
                        return;
                    } else {
                        redrawBubbles();
                        await sleep(500);
                    }
                } else if (vals[idx] <= vals[val]) {
                    idx = val;
                }
            }
        }
        idx = 0;
    }
    redrawBubbles();
    console.debug("Finished bubble sorting");
}
